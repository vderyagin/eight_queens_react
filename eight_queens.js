/* @flow */
/*global React, document, location, Worker */

var D = React.DOM;
var worker = new Worker('/dist/solver.js');

var boardSpecs = {
  minSize: 4,
  maxSize: 20,
  defaultSize: 8
};

var initialBoardSize = function () {
  var size = parseInt(location.hash.substring(1), 10);

  if (!(size >= boardSpecs.minSize && size <= boardSpecs.maxSize)) {
    size = boardSpecs.defaultSize;
  }

  return size;
};

var Cell = React.createClass({
  render: function () {
    var classes = ['cell'];
    classes.push(this.color());

    if (this.props.containsQueen) {
      classes.push('queen');
    }

    return D.div({'className': classes.join(' ')});
  },

  color: function () {
    return (this.props.column % 2 === this.props.row % 2) ? 'white' : 'black';
  }
});

var Row = React.createClass({
  render: function () {
    var cells = [];

    for (var i = 0; i < this.props.size; i += 1) {
      cells[i] = Cell({
        key: i.toString(),
        column: i,
        row: this.props.rowNumber,
        containsQueen: this.props.pred(this.props.rowNumber, i)
      });
    }

    return D.div({className: 'row'}, cells);
  }
});

var Board = React.createClass({
  isQueen: function (rowNumber, columnNumber) {
    return this.props.solution[rowNumber] === columnNumber;
  },

  render: function () {
    var rows = [];

    for (var i = 0; i < this.props.size; i += 1) {
      rows[i] = Row({
        size: this.props.size,
        className: 'row' + i.toString(),
        key: 'row' + i.toString(),
        rowNumber: i,
        pred: this.isQueen
      });
    }

    return D.div({className: 'board'}, rows);
  }
});

var StartButton = React.createClass({
  render: function () {
    return D.button({
      className: 'new-solution',
      onClick: this.props.handleClick
    }, 'Another Solution');
  }
});

var BoardSizeControl = React.createClass({
  size: function () {
    var sizeStr = this.props.currentSize.toString();
    return 'size: ' + sizeStr + '×' + sizeStr;
  },

  render: function () {
    return D.input({
      type: 'range',
      className: 'hint--right',
      'data-hint': this.size(),
      min: boardSpecs.minSize,
      max: boardSpecs.maxSize,
      defaultValue: this.props.defaultValue,
      onChange: this.props.handleChange
    });
  }
});

var Game = React.createClass({
  getInitialState: function () {
    return {
      prevSize: null,
      boardSize: this.props.initialSize,
      solution: []
    };
  },

  componentDidMount: function () {
    worker.onmessage = function (event) {
      if (this.isMounted()) {
        this.setState({
          solution: event.data.solution
        });
      }
    }.bind(this);

    location.hash = this.state.boardSize.toString();

    worker.postMessage(this.state.boardSize);
  },

  handleBoardSizeChange: function (e) {
    var newSize = parseInt(e.target.value);

    if (newSize === this.state.prevSize) {
      return;
    }

    this.setState({
      prevSize: this.state.boardSize,
      boardSize: newSize
    });

    location.hash = newSize.toString();

    worker.postMessage(newSize);
  },

  updateSolution: function () {
    worker.postMessage(this.state.boardSize);
  },

  render: function () {
    return D.div(null,
                 BoardSizeControl({
                   currentSize: this.state.boardSize,
                   defaultValue: this.state.boardSize,
                   handleChange: this.handleBoardSizeChange
                 }),
                 StartButton({
                   handleClick: this.updateSolution
                 }),
                 Board({
                   size: this.state.boardSize,
                   solution: this.state.solution
                 })
                );
  }
});

React.renderComponent(Game({
  initialSize: initialBoardSize()
}), document.getElementById('app'));
