/* @flow */

/*exported solve, solutionsEqual */
/*global self */

var clashExists = function (x1, y1, x2, y2) {
  if ((x1 === x2) ||
      (y1 === y2) ||
      (x1 - y1 === x2 - y2) ||
      (x1 + y1 === x2 + y2)) {
    return true;
  }

  return false;
};

var correctSolution = function (solution) {
  for (var idx = 0; idx < solution.length; idx += 1) {
    for (var j = idx + 1; j < solution.length; j += 1) {
      var queen = solution[idx];
      var otherQueen = solution[j];
      if (clashExists(queen[0], queen[1], otherQueen[0], otherQueen[1])) {
        return false;
      }
    }
  }

  return true;
};

var randomRowOrder = function (rowCount) {
  var order = [];

  for (var i = 0; i < rowCount; i += 1) {
    order[i] = i;
  }

  return order.sort(function () {
    return 0.5 - Math.random();
  });
};

var solutionToArray = function (solution) {
  var ary = [];

  solution.forEach(function (queen) {
    ary[queen[1]] = queen[0];
  });

  return ary;
};

var lastSolution = [];

var solutionsEqual = function (s1, s2) {
  if (s1.length !== s2.length) {
    return false;
  }

  for (var i = 0; i < s1.length; i += 1) {
    if (s1[i] !== s2[i]) {
      return false;
    }
  }

  return true;
};

var solve = function (event) {
  var size = event.data;

  var rowsOrder = randomRowOrder(size);
  var solution = [];

  for (var i = 0; i < size; i += 1) {
    solution[i] = [0, i];
  }


  var partialSolution = function (idx) {
    var completedRowIdxs = rowsOrder.slice(0, idx + 1);

    return completedRowIdxs.map(function (y) {
      return [solution[y][0], y];
    });
  };

  var rowPosition = function (idx) {
    return solution[rowsOrder[idx]][0];
  };

  var resetRowPosition = function (idx) {
    solution[rowsOrder[idx]][0] = 0;
  };

  var incrementRowPosition = function (idx) {
    solution[rowsOrder[idx]][0] += 1;
  };

  var idx = 0;
  var lastIdx = idx;

  while(!correctSolution(solution)) {
    if (correctSolution(partialSolution(idx))) {
      idx += 1;

      if (idx > lastIdx) {
        self.postMessage({
          complete: false,
          solution: solutionToArray(solution)
        });
        lastIdx = idx;
      }
    } else {
      while (rowPosition(idx) === size - 1) {
        resetRowPosition(idx);
        idx -= 1;
      }
      incrementRowPosition(idx);
    }
  }

  var finalSolution = solutionToArray(solution);
  if (solutionsEqual(finalSolution, lastSolution)) {
    solve({data: size});
  } else {
    lastSolution = finalSolution;
    self.postMessage({
      complete: true,
      solution: finalSolution
    });
  }
};

self.addEventListener('message', solve, false);
