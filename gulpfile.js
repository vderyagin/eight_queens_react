/* @flow */
/*global require */

var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat');
var jsHint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');

var styleSheets = [
  'bower_components/normalize-css/normalize.css',
  'bower_components/hint.css/hint.base.css',
  'eight_queens.css',
  'button.css',
];

var scripts = [
  'bower_components/react/react.js',
  'eight_queens.js',
];

var worker = 'solver.js';

gulp.task('css', function () {
  return gulp.src(styleSheets)
    .pipe(minifyCSS())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./dist'));
});

gulp.task('js', function () {
  return gulp.src(scripts)
    .pipe(uglify())
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./dist'));
});

gulp.task('worker', function () {
  return gulp.src(worker)
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));
});

gulp.task('lint', function () {
  return gulp.src('*.js')
    .pipe(jsHint())
    .pipe(jsHint.reporter('default'));
});

gulp.task('default', function () {
  gulp.start(['css', 'js', 'worker', 'lint']);
});

gulp.task('watch', function () {
  gulp.watch(styleSheets, ['css']);
  gulp.watch(worker, ['worker', 'lint']);
  gulp.watch(scripts, ['js', 'lint']);

  livereload.listen();
  gulp.watch(['./dist/**']).on('change', livereload.changed);
});
